Introduction
-----------------------
This module allows sites using the Payment[1] module to accept payments through
the Finnish payment provider Checkout[2].

Configuration
-----------------------
- Enable the module.
- Add and configure a Payment method using Checkout.fi at
  admin/config/services/payment/method/add
  
- Optionally, configure response messages at
  admin/config/services/payment/checkoutfi_messages

Disclaimer
-----------------------
This module is provided as-is and is not in any way affiliated with Checkout.fi.

[1] https://drupal.org/project/payment
[2] http://www.checkout.fi